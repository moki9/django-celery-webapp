apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "introspect-webapp.fullname" . }}-webapp
  labels:
    app.kubernetes.io/name: {{ include "introspect-webapp.name" . }}
    helm.sh/chart: {{ include "introspect-webapp.chart" . }}
    app.kubernetes.io/instance: {{ .Release.Name }}
    app.kubernetes.io/managed-by: {{ .Release.Service }}
    app.introspectdev.io/role: webapp
spec:
  replicas: {{ .Values.webapp.replicaCount }}
  selector:
    matchLabels:
      app.kubernetes.io/name: {{ include "introspect-webapp.name" . }}
      app.kubernetes.io/instance: {{ .Release.Name }}
      app.introspectdev.io/role: webapp
  template:
    metadata:
      labels:
        app.kubernetes.io/name: {{ include "introspect-webapp.name" . }}
        app.kubernetes.io/instance: {{ .Release.Name }}
        app.introspectdev.io/role: webapp
    spec:
      initContainers:
        - name: {{ .Chart.Name }}-django-migrate
          image: "{{ .Values.app.image.repository }}:{{ .Values.app.image.tag }}"
          imagePullPolicy: {{ .Values.app.image.pullPolicy }}
          command:
            - /bin/bash
            - -c
          args: {{ .Values.app.initArgs }}
          envFrom:
            - configMapRef:
                name: {{ template "introspect-webapp.fullname" . }}
            - secretRef:
                name: {{ template "introspect-webapp.fullname" . }}
      containers:
        - name: {{ .Chart.Name }}-webapp
          image: "{{ .Values.app.image.repository }}:{{ .Values.app.image.tag }}"
          imagePullPolicy: {{ .Values.app.image.pullPolicy }}
          envFrom:
            - configMapRef:
                name: {{ template "introspect-webapp.fullname" . }}
            - secretRef:
                name: {{ template "introspect-webapp.fullname" . }}
          cmd:
            - /bin/bash
            - -c
          args:
            - gunicorn
            - --workers=3
            - --bind=0.0.0.0:8000
            - {{ .Values.app.wsgiApp }}
          ports:
            - name: http
              containerPort: 8000
        {{- if .Values.probe.enabled }}
          livenessProbe:
            initialDelaySeconds: 30
            httpGet:
              path: {{ .Values.probe.path }}
              port: {{ .Values.probe.port }}
          readinessProbe:
            initialDelaySeconds: 30
            httpGet:
              path: {{ .Values.probe.path }}
              port: {{ .Values.probe.port }}
        {{- end }}
          volumeMounts:
            - mountPath: {{ .Values.app.mediaPath }}
              name: media-path
          resources:
            {{- toYaml .Values.webapp.resources | nindent 12 }}
      {{- with .Values.webapp.nodeSelector }}
      nodeSelector:
        {{- toYaml . | nindent 8 }}
      {{- end }}
    {{- with .Values.webapp.affinity }}
      affinity:
        {{- toYaml . | nindent 8 }}
    {{- end }}
    {{- with .Values.webapp.tolerations }}
      tolerations:
        {{- toYaml . | nindent 8 }}
    {{- end }}
      volumes:
        - name: media-path
        {{- if .Values.app.sharedMount.enabled }}
          persistentVolumeClaim:
            claimName: {{ if .Values.app.sharedMount.existingClaim }}{{ .Values.app.sharedMount.existingClaim }}{{- else }}{{ template "introspect-webapp.fullname" . }}{{- end }}
        {{- else }}
          emptyDir: {}
        {{- end }}
